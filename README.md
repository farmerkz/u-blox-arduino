# u-blox-Arduino

A library for managing u-blox m8 GNSS receivers, particularly suited for precision timing applications.

## Background

This is a library for managing the u-blox M8 series of GNSS (“GPS”) receiver modules. It uses the u-blox UBX binary protocol exclusively.
