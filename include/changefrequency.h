#include <Arduino.h>

#ifndef __changefrequency_H__
#define __changefrequency_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to change nav period to requested ms (1000 = 1 Hz)
void changeFrequency(uint16_t ms)
{
  // CFG-RATE packet
  byte packet[] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x08, // id
      0x06, // length
      0x00, // length
      0x64, // payload
      0x00, // payload
      0x01, // payload
      0x00, // payload
      0x01, // payload
      0x00, // payload
      0x00, // CK_A
      0x00, // CK_B
  };

  uint16_t *msp = (uint16_t *)&packet[6];
  *msp = ms;

  byte packetSize = sizeof(packet);

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
