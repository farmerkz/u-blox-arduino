#include <Arduino.h>

#ifndef __pollsatnavparameters_H__
#define __pollsatnavparameters_H__

extern void sendPacket(byte *packet, byte len);

// Надо добавить описание
void pollSatNavParameters()
{
  byte packet[] =
      {
          0xB5, // sync char 1
          0x62, // sync char 2
          0x01, // class
          0x35, // id
          0x00, // length
          0x00, // length
          0x00, // CK_A
          0x00, // CK_B
      };

  byte packetSize = sizeof(packet);

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
