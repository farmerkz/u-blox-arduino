#include <Arduino.h>

#ifndef __polltimepulseparameters_H__
#define __polltimepulseparameters_H__

extern void sendPacket(byte *packet, byte len);

// Нужно дописать описание
void pollTimePulseParameters()
{
  byte packet[] =
      {
          0xB5, // sync char 1
          0x62, // sync char 2
          0x06, // class
          0x31, // id
          0x00, // length
          0x00, // length
          0x00, // CK_A
          0x00, // CK_B
      };

  byte packetSize = sizeof(packet);

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
