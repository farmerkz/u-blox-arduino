#include <Arduino.h>
#include "u-blox-arduino.h"

#ifndef __sendtimepulseparameters_H__
#define __sendtimepulseparameters_H__

extern void sendPacket(byte *packet, byte len);

// Нужно дописать описание
void sendTimePulseParameters(uint32_t flags)
{
  byte packet[sizeof(_cfgtp5) + 4];

  packet[0] = 0xB5; // sync char 1
  packet[1] = 0x62; // sync char 2
  packet[2] = cfgtp5hdr.cl;
  packet[3] = cfgtp5hdr.id;
  uint16_t *i16p = (uint16_t *)&(packet[4]);
  *i16p = cfgtp5hdr.length;

  _cfgtp5 *p = (_cfgtp5 *)&(packet[2]);

  p->tpIdx = 0;   // 0 = TIMEPULSE, 1 = TIMEPULSE2
  p->version = 0; // 1 for this version
  p->reserved = 0;
  p->antCableDelay = 50;         // antenna cable delay in ns
  p->rfGroupDelay = 0;           // RF group delay in ns
  p->freqPeriod = 1000000;       // frequency in Hz or period in us
  p->freqPeriodLock = 1000000;   // frequency or period when locked
  p->pulseLenRatio = 500000;     // pulse length or duty cycle
  p->pulseLenRatioLock = 100000; // pulse length or duty cycle when locked
  p->userConfigDelay = 0;        // user configurable time pulse delay in ns
  p->flags = flags;

  byte packetSize = sizeof(packet);

  packet[packetSize - 1] = 0;
  packet[packetSize - 2] = 0;

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
