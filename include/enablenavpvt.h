#include <Arduino.h>

#ifndef __enablenavpvt_H__
#define __enablenavpvt_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to enable NAV-PVT messages
void enableNavPvt()
{
  // CFG-MSG packet
  byte packet[] PROGMEM = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x01, // id
      0x03, // length
      0x00, // length
      0x01, // payload
      0x07, // payload
      0x01, // payload
      0x13, // CK_A
      0x51, // CK_B
  };

  sendPacket(packet, sizeof(packet));
}
#endif
