#include <Arduino.h>

#ifndef __changebaudrate_H__
#define __changebaudrate_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to change baudrate (in bits/second)
void changeBaudrate(uint32_t baudRate)
{
  // CFG-PRT packet
  byte packet[] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x00, // id
      0x14, // length
      0x00, // length
      0x01, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0xD0, // payload
      0x08, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0xC2, // payload
      0x01, // payload
      0x00, // payload
      0x07, // payload
      0x00, // payload
      0x03, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // CK_A
      0x00, // CK_B
  };

  uint8_t *mbr = (uint8_t *)&baudRate;
  //*mbr = baudRate;
  for (int i = 0; i < 4; i++)
    packet[i + 14] = *(mbr++);

  byte packetSize = sizeof(packet);

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
