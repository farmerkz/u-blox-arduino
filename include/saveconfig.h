#include <Arduino.h>

#ifndef __saveconfig_H__
#define __saveconfig_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to save current configuration (BBR/Flash)
void saveConfig()
{
  // CFG-CFG packet
  byte packet[] PROGMEM = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x09, // id
      0x0D, // length
      0x00, // length
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0xFF, // payload
      0xFF, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x03, // payload
      0x1D, // CK_A
      0xAB, // CK_B
  };

  sendPacket(packet, sizeof(packet));
}
#endif
