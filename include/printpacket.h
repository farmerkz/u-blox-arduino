#include <Arduino.h>

#ifndef __printpacket_H__
#define __printpacket_H__

// Print the packet specified to the PC  in a hexadecimal form, for debugging
void printPacket(byte *packet, byte len)
{
  char temp[3];

  for (byte i = 0; i < len; i++)
  {
    sprintf(temp, "%.2X", packet[i]);
    Serial.print(temp);

    if (i != len - 1)
    {
      Serial.print(' ');
    }
  }

  Serial.println();
}
#endif
