#include <Arduino.h>

#ifndef __enablenavsat_H__
#define __enablenavsat_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to enable NAV-SAT messages
void enableNavSat()
{
  // CFG-MSG packet
  byte packet[] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x01, // id
      0x03, // length
      0x00, // length
      0x01, // payload
      0x35, // payload
      0x01, // payload
      0x00, // CK_A
      0x00, // CK_B
  };

  byte packetSize = sizeof(packet);

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
