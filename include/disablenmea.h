#include <Arduino.h>

#ifndef __disablenmea_H__
#define __disablenmea_H__

extern void sendPacket(byte *packet, byte len);

// Send a set of packets to the receiver to disable NMEA messages
void disableNmea()
{
  // Array of two bytes for CFG-MSG packets payload
  const byte messages[][2] PROGMEM = {
      {0xF0, 0x0A},
      {0xF0, 0x09},
      {0xF0, 0x00},
      {0xF0, 0x01},
      {0xF0, 0x0D},
      {0xF0, 0x06},
      {0xF0, 0x02},
      {0xF0, 0x07},
      {0xF0, 0x03},
      {0xF0, 0x04},
      {0xF0, 0x0E},
      {0xF0, 0x0F},
      {0xF0, 0x05},
      {0xF0, 0x08},
      {0xF1, 0x00},
      {0xF1, 0x01},
      {0xF1, 0x03},
      {0xF1, 0x04},
      {0xF1, 0x05},
      {0xF1, 0x06},
  };

  // CFG-MSG packet buffer
  byte packet[] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x01, // id
      0x03, // length
      0x00, // length
      0x00, // payload (first byte from messages array element)
      0x00, // payload (second byte from messages array element)
      0x00, // payload (not changed in the case)
      0x00, // CK_A
      0x00, // CK_B
  };
  byte packetSize = sizeof(packet);

  // Offset to the place where payload starts
  byte payloadOffset = 6;

  // Iterate over the messages array
  for (byte i = 0; i < sizeof(messages) / sizeof(*messages); i++)
  {
    // Copy two bytes of payload to the packet buffer
    for (byte j = 0; j < sizeof(*messages); j++)
    {
      packet[payloadOffset + j] = messages[i][j];
    }

    // Set checksum bytes to the null
    packet[packetSize - 2] = 0x00;
    packet[packetSize - 1] = 0x00;

    // Calculate checksum over the packet buffer excluding sync (first two) and checksum chars (last two)
    for (byte j = 0; j < packetSize - 4; j++)
    {
      packet[packetSize - 2] += packet[2 + j];
      packet[packetSize - 1] += packet[packetSize - 2];
    }
    sendPacket(packet, packetSize);
  }
}
#endif
