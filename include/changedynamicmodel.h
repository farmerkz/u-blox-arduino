#include <Arduino.h>

#ifndef __changedynamicmodel_H__
#define __changedynamicmodel_H__

extern void sendPacket(byte *packet, byte len);

// Send a packet to the receiver to change dynamic model
void changeDynamicModel(uint8_t model)
{
  // CFG-NAV5 packet
  byte packet[44] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x24, // id
      0x24, // length
      0x00, // length
      0x01, // payload - choose dynamic model
      0x00,
      0x00 // payload - dynModel
  };

  uint8_t *dmp = (uint8_t *)&packet[8];
  *dmp = model;

  byte packetSize = sizeof(packet);

  packet[packetSize - 2] = 0;
  packet[packetSize - 1] = 0;

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(packet, sizeof(packet));
}
#endif
